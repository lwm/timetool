﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace TimeTool
{
    public class ReservationViewModel : ViewModel<ReservationEntity>
    {
        private string name;
        private string comment;
        private DateTime reservationStart;
        private DateTime reservationEnd;

        public long Id { get { return model.Id; } }
        public DateTime LastChange { get { return model.LastChange; } }
        public string ReservableName { get; }

        public string Name { get { return name; } set { Set(ref name, value); } }
        public string Comment { get { return comment; } set { Set(ref comment, value); } }
        public DateTime ReservationStart { get { return reservationStart; } set { Set(ref reservationStart, value); } }
        public DateTime ReservationEnd { get { return reservationEnd; } set { Set(ref reservationEnd, value); } }

        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

        public ICommand EditCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public ReservationViewModel(Database db, ReservationEntity model, MainViewModel mainViewModel, string reservableName)
            : base(db, model)
        {
            this.ReservableName = reservableName;

            this.name = model.Name;
            this.comment = model.Comment;
            this.reservationStart = model.ReservationStart;
            this.reservationEnd = model.ReservationEnd;

            this.SaveCommand = new Command<Window>(window =>
            {
                this.model.Name = name;
                this.model.Comment = comment;
                this.model.LastChange = DateTime.Now;
                this.model.ReservationStart = reservationStart;
                this.model.ReservationEnd = reservationEnd;

                if (this.model.Id == 0)
                {
                    this.db.GetContext().Reservations.Add(this.model);
                }

                this.db.GetContext().SaveChanges();
                window.Close();
                mainViewModel.Refresh();
            });

            this.CancelCommand = new Command<Window>(window =>
            {
                this.Name = model.Name;
                this.Comment = model.Comment;
                this.ReservationStart = model.ReservationStart;
                this.ReservationEnd = model.ReservationEnd;
                window.Close();
            });

            this.EditCommand = new Command<object>(x =>
            {
                var window = new ReservationWindow()
                {
                    DataContext = this,
                    Owner = MainWindow.instance
                };
                window.ShowDialog();
            });

            this.DeleteCommand = new Command<object>(x =>
            {
                MainWindow.instance.ShowMessageAsync("Löschen?", $"Eintrag von {Name} löschen?", MessageDialogStyle.AffirmativeAndNegative).ContinueWith(t =>
                {
                    if (t.Result == MessageDialogResult.Affirmative)
                    {
                        this.db.GetContext().Reservations.Remove(this.model);
                        this.db.GetContext().SaveChanges();
                        mainViewModel.Refresh();
                    }
                });                
            });
        }

        public override string ToString()
        {
            return $"{Id} {Name}";
        }
    }
}
