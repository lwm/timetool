﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TimeTool
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void Set<X>(ref X target, X value, [CallerMemberName] string caller = "")
        {
            target = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }

        protected void Raise([CallerMemberName] string caller = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }
    }

    public class ViewModel<T> : ViewModel
    {
        protected readonly Database db;
        protected T model;

        public T Model => model;

        public ViewModel(Database db, T model)
        {
            this.db = db;
            this.model = model;
        }
    }
}
