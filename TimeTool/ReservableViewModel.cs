﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;

namespace TimeTool
{
    public class ReservableViewModel : ViewModel<ReservableEntity>
    {
        private static string userDisplayName;

        static ReservableViewModel()
        {
            userDisplayName = Environment.UserName;

            Task.Run(() =>
            {
                try
                {
                    var name = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;
                    var braceIndex = name.IndexOf('(');
                    if (braceIndex > 0)
                    {
                        name = name.Substring(0, braceIndex).Trim();
                    }
                    userDisplayName = name;
                }
                catch
                {
                    // ignored
                }
            });            
        }

        private string name;
        private string comment;
        private string category;

        public long Id { get { return model.Id; } }
        public string Name { get { return name; } set { Set(ref name, value); } }
        public string Comment { get { return comment; } set { Set(ref comment, value); } }
        public string Category { get { return category; } set { Set(ref category, value); } }
        public bool IsInUse => Reservations.Any(r => r.ReservationStart < DateTime.Now && r.ReservationEnd > DateTime.Now);

        public ObservableCollection<ReservationViewModel> Reservations { get; }
        public ICommand NewReservationCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public ICommand EditCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public ReservableViewModel(Database db, ReservableEntity model, IEnumerable<ReservationViewModel> reservations, MainViewModel mainViewModel)
            : base(db, model)
        {
            this.name = model.Name;
            this.comment = model.Comment;
            this.category = model.Category;

            this.Reservations = new ObservableCollection<ReservationViewModel>(reservations);
            this.NewReservationCommand = new Command<object>(a =>
            {
                var time = (DateTime.Now - DateTime.Now.Date);
                var m = new ReservationEntity()
                {
                    Name = userDisplayName,
                    ReservableId = Id,
                    ReservationStart = mainViewModel.CurrentDate.Value + time,
                    ReservationEnd = mainViewModel.CurrentDate.Value + time + TimeSpan.FromHours(1)
                };
                var window = new ReservationWindow()
                {
                    Owner = MainWindow.instance,
                    DataContext = new ReservationViewModel(db, m, mainViewModel, this.Name)
                };
                window.ShowDialog();
                mainViewModel.Refresh();
            });

            this.SaveCommand = new Command<MetroWindow>(window =>
            {
                if (string.IsNullOrEmpty(this.Name))
                {
                    window.ShowMessageAsync("Fehler", "Name ist leer. Y U DO DIS?");
                    return;
                }

                this.model.Name = name;
                this.model.Comment = comment;
                this.model.Category = string.IsNullOrEmpty(category) ? null : category;

                if (this.model.Id == 0)
                {
                    this.db.GetContext().Reservables.Add(this.model);
                }

                this.db.GetContext().SaveChanges();
                window.Close();
                mainViewModel.Refresh();
            });

            this.CancelCommand = new Command<Window>(window =>
            {
                this.Name = model.Name;
                window.Close();
            });

            this.EditCommand = new Command<object>(x =>
            {
                var window = new ReservableWindow()
                {
                    Owner = MainWindow.instance,
                    DataContext = new ReservableViewModel(db, this.model, new ReservationViewModel[0], mainViewModel)
                };
                window.ShowDialog();
                mainViewModel.Refresh();
            });

            this.DeleteCommand = new Command<object>(x =>
            {
                MainWindow.instance.ShowMessageAsync("Löschen?", $"Spalte {name} löschen?", MessageDialogStyle.AffirmativeAndNegative).ContinueWith(t =>
                {
                    if (t.Result == MessageDialogResult.Affirmative)
                    {
                        this.db.GetContext().Reservables.Remove(this.model);
                        this.db.GetContext().SaveChanges();
                        mainViewModel.Refresh();
                    }
                });
            });
        }

        public override string ToString()
        {
            return $"{Id} {Name}";
        }
    }
}
