﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace TimeTool
{
    /// <summary />
    public sealed class BooleanToVisibilityConverter : IValueConverter
    {
        /// <summary />
        public Visibility True { get; set; }
        /// <summary />
        public Visibility False { get; set; }

        /// <summary />
        public BooleanToVisibilityConverter()
        {
            True = Visibility.Visible;
            False = Visibility.Collapsed;
        }

        /// <summary />
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return False;
            var v = (bool)value;
            return v ? True : False;
        }

        /// <summary />
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
