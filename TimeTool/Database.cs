﻿using Microsoft.Data.Entity;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TimeTool
{
    public class Database
    {
        private SqliteContext context;

        public Database()
        {
        }

        public void Initialize()
        {
            if (context != null)
                context.Dispose();

            this.context = new SqliteContext();
            this.context.Database.EnsureCreated();
        }

        public SqliteContext GetContext()
        {
            if (context == null)
                Initialize();

            return context;
        }
    }

    public class SqliteContext : DbContext
    {
        public DbSet<ReservableEntity> Reservables { get; set; }
        public DbSet<ReservationEntity> Reservations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!File.Exists(Settings1.Default.SqliteFile))
            {
                var binDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var file = Directory.EnumerateFiles(binDir, "*.sqlite").FirstOrDefault();
                if (file != null)
                {
                    Settings1.Default.SqliteFile = file;
                    Settings1.Default.Save();
                }
            }

            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = Settings1.Default.SqliteFile };
            var connectionString = connectionStringBuilder.ToString();

            Migrations.Migrator.MigrateUp(connectionString);

            var connection = new SqliteConnection(connectionString);
            optionsBuilder.UseSqlite(connection);
        }
    }
}
