﻿using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TimeTool.Migrations
{
    public static class Migrator
    {
        private class MigrationOptions : IMigrationProcessorOptions
        {
            public bool PreviewOnly => false;
            public string ProviderSwitches => "";
            public int Timeout => 0;
        }

        public static void MigrateUp(string connectionString)
        {
            var options = new MigrationOptions();
            var factory = new FluentMigrator.Runner.Processors.SQLite.SQLiteProcessorFactory();
            var assembly = Assembly.GetExecutingAssembly();
            var announcer = new TextWriterAnnouncer(s => System.Diagnostics.Debug.WriteLine(s));
            var migrationContext = new RunnerContext(announcer);
            var processor = factory.Create(connectionString, announcer, options);
            var runner = new MigrationRunner(assembly, migrationContext, processor);
            runner.MigrateUp();
        }
    }
}
