﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls.Dialogs;
using System.IO;

namespace TimeTool
{
    public class MainViewModel : ViewModel
    {
        private readonly Database db;

        private static readonly string allCategories = "<Alle>";

        private DateTime currentDate = DateTime.Today;
        private ObservableCollection<ReservableViewModel> reservables;
        private ObservableCollection<string> categories;
        private string selectedCategory = string.IsNullOrEmpty(Settings1.Default.SelectedCategory) ? allCategories : Settings1.Default.SelectedCategory;
        private bool isLoading = false;
        private string currentDbName = "";

        public DateTime? CurrentDate
        {
            get { return currentDate; }
            set
            {
                Set(ref currentDate, value ?? DateTime.Today);
                Refresh();
            }
        }
        public ObservableCollection<ReservableViewModel> Reservables { get { return this.reservables; } private set { Set(ref this.reservables, value); } }
        public ObservableCollection<string> Categories { get { return this.categories; } private set { Set(ref this.categories, value); } }
        public string SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                Set(ref selectedCategory, value);
                Refresh();
                Settings1.Default.SelectedCategory = value;
                Settings1.Default.Save();
            }
        }
        public string CurrentDbName { get { return currentDbName; } set { Set(ref currentDbName, value); } }

        public bool IsLoading { get { return isLoading; } set { Set(ref isLoading, value); } }

        public ICommand NewDbCommand { get; private set; }
        public ICommand SelectDbCommand { get; private set; }

        public ICommand NewReservableCommand { get; private set; }
        public ICommand RefreshCommand { get; private set; }

        public MainViewModel(Database db)
        {
            this.db = db;
            Refresh();

            this.NewDbCommand = new Command<object>(a =>
            {
                var dialog = new SaveFileDialog()
                {
                    Filter = "SQLite DB|*.sqlite"
                };
                if (dialog.ShowDialog().GetValueOrDefault())
                {
                    Settings1.Default.SqliteFile = dialog.FileName;
                    Settings1.Default.Save();
                    this.db.Initialize();
                    this.Refresh();
                }
            });

            this.SelectDbCommand = new Command<object>(a =>
            {
                var dialog = new OpenFileDialog()
                {
                    Filter = "SQLite DB|*.sqlite"
                };
                if (dialog.ShowDialog().GetValueOrDefault())
                {
                    Settings1.Default.SqliteFile = dialog.FileName;
                    Settings1.Default.Save();
                    this.db.Initialize();
                    this.Refresh();
                }
            });

            this.NewReservableCommand = new Command<object>(a =>
            {
                var m = new ReservableEntity()
                {
                    Name = ""
                };
                var window = new ReservableWindow()
                {
                    Owner = MainWindow.instance,
                    DataContext = new ReservableViewModel(db, m, new ReservationViewModel[0], this)
                };
                window.ShowDialog();
                Refresh();
            });

            this.RefreshCommand = new Command<object>(a =>
            {
                Refresh();
            });
        }

        public void Refresh()
        {
            if (this.IsLoading)
                return;

            this.IsLoading = true;
            Task.Run(() =>
            {
                var context = db.GetContext();
                var newReservables = new ObservableCollection<ReservableViewModel>();
                var allReservables = context.Reservables.ToList();

                foreach (var reservable in allReservables)
                {
                    if (this.selectedCategory != allCategories && this.selectedCategory != reservable.Category)
                        continue;

                    var tomorrowTicks = currentDate + TimeSpan.FromDays(1);
                    var reservations = context.Reservations
                        .Where(r => r.ReservableId == reservable.Id)
                        .Where(r => r.ReservationStart.Date <= currentDate && r.ReservationEnd >= currentDate)
                        .OrderBy(r => r.ReservationStart)
                        .ToList();

                    var reservationViewModels = reservations.Select(r => new ReservationViewModel(db, r, this, reservable.Name));
                    var viewModel = new ReservableViewModel(db, reservable, reservationViewModels, this);
                    newReservables.Add(viewModel);
                }
                this.Reservables = newReservables;

                var categoryNames = allReservables
                    .Select(r => r.Category)
                    .Where(s => !string.IsNullOrEmpty(s))
                    .Distinct()
                    .ToList();
                categoryNames.Insert(0, allCategories);
                this.Categories = new ObservableCollection<string>(categoryNames);

                this.CurrentDbName = Path.GetFileName(Settings1.Default.SqliteFile);

                this.IsLoading = false;
            }).ContinueWith(t =>
            {
                if(t.Exception != null)
                {
                    MainWindow.instance.Dispatcher.Invoke(async () =>
                    {
                        await MainWindow.instance.ShowMessageAsync("Error", t.Exception.ToString());
                        this.IsLoading = false;
                    });
                }
            });
        }
    }
}
